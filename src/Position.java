public class Position {
    private Integer row;
    private Integer col;

    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public Integer getRow() {
        return row;
    }

    public Integer getCol() {
        return col;
    }
}
