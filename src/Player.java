public abstract class Player {
    private String name;

    public Player(String name) {
        this.name = name;
    }

    public abstract Action nextAction();
    public String getName() {
        return name;
    }
}
