import org.jgrapht.*;
import org.jgrapht.graph.*;

import java.awt.*;
import java.util.*;

public class GameBoard {
    private Piece redPiece;
    private Piece blackPiece;
    private Graph<Cell, DefaultEdge> board;
    private Piece[] pieces = new Piece[2];
    private Map<Integer, HashSet<Wall>> wallStatus = new TreeMap<>();
    HashSet<Wall> set1 = new HashSet<>();
    HashSet<Wall> set2 = new HashSet<>();

    public GameBoard() {
        redPiece = new Piece(new Position(0,4));
        blackPiece = new Piece(new Position(8,4));
        pieces[0]=redPiece;
        pieces[1]=blackPiece;
        set1.add(new Wall(new Position(8,8),Direction.HORIZENTAL));
        set2.add(new Wall(new Position(8,8),Direction.HORIZENTAL));
        wallStatus.put(0, set1);
        wallStatus.put(1, set2);
        board = new SimpleGraph<>(DefaultEdge.class);
        Cell.getCells().forEach(cell -> board.addVertex(cell));
        board.vertexSet().forEach(cell -> {
            if (cell.getRow() < 8) {
                board.addEdge(cell, cell.getLowerCell());
            }
            if(cell.getCol()<8){
                board.addEdge(cell, cell.getRightCell());
            }
        });

    }

    public Piece[] getPieces() {
        return pieces;
    }

    public Graph<Cell, DefaultEdge> getBoard() {
        return board;
    }
    public Piece getRedPiece() {
        return redPiece;
    }

    public Piece getBlackPiece() {
        return blackPiece;
    }

    public Map<Integer, HashSet<Wall>> getWallStatus() {
        return wallStatus;
    }
}


