import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class GUIMain extends Application {
    Button up;
    Button down;
    Button left;
    Button right;
    Rectangle[][] cellsRectangles;
    Rectangle[][] horizontal;
    Rectangle[][] vertical;
    Stage primaryStage;
    Game game = new Game();
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        VBox vbox = new VBox();
        Button classicButton = new Button("Classic");
        classicButton.setStyle("-fx-background-color: #f52887");
        classicButton.setPrefWidth(250);
        classicButton.setPrefHeight(80);

        Label label = new Label("My Quoridors");
        label.setTextFill(Color.GRAY);
        vbox.getChildren().addAll(label, classicButton);

        vbox.setSpacing(50);
        vbox.setStyle("-fx-background-color: #e8adaa");
        vbox.setAlignment(Pos.CENTER);



        primaryStage.setTitle("quoridors");
        Scene scene = new Scene(vbox, 300,300);
        scene.setFill(Color.YELLOW);
        primaryStage.setScene(scene);
        primaryStage.show();
        classicButton.setOnMouseClicked(event ->{
            Scene scene1 = new Scene(nameBox(), 300,300);
            primaryStage.setScene(scene1);
        });

    }
    public Pane nameBox(){
        VBox vBox = new VBox();
        vBox.setStyle("-fx-background-color: #e8adaa");
        HBox name1 = new HBox();
        HBox name2 = new HBox();
        HBox okbox = new HBox();
        Label player1label = new Label("Player 1: ");
        TextField player1name = new TextField();
        Label player2label = new Label("Player2: ");
        TextField player2name = new TextField();
        Button ok = new Button("Ok");
        ok.setStyle("-fx-background-color: #f52887");
        ok.setPrefWidth(60);
        ok.setPrefHeight(25);
        okbox.getChildren().add(ok);
        okbox.setAlignment(Pos.CENTER);
        name1.getChildren().addAll(player1label, player1name);
        name1.setPadding(new Insets(20));
        name1.setSpacing(30);
        name2.getChildren().addAll(player2label, player2name);
        name2.setPadding(new Insets(20));
        name2.setSpacing(30);
        vBox.getChildren().addAll(name1, name2, okbox);
        ok.setOnMouseClicked(event -> {
            Player player1;
            Player player2;
            if(player1name.getText().length() == 0){
                player1 = new HumanPlayer("Ananymous1")  ;
            }else{
                player1 = new HumanPlayer(player1name.getText());
            }
            if(player2name.getText().length() == 0){
                player2 = new HumanPlayer("Ananymous2")  ;
            }else{
                player2 = new HumanPlayer(player2name.getText());
            }
            Scene scene2 = new Scene(makeBoard(player1, player2), 800, 800);
            primaryStage.setScene(scene2);
        });
        return vBox;
    }

    public Pane makeBoard(Player p1, Player p2){
        BorderPane borderPane = new BorderPane();
        borderPane.setStyle("-fx-background-color: #e8adaa");
        VBox leftSide = new VBox();
        borderPane.setLeft(leftSide);
        Label label1 = new Label(p1.getName());
        Label label2 = new Label( p2.getName());
        label1.setTextFill(Color.ORANGERED);
        label2.setTextFill(Color.DARKOLIVEGREEN);
        up = new Button("up");
        up.setStyle( "-fx-background-color: #f52887");
        left = new Button("left");
        left.setStyle( "-fx-background-color: #f52887");
        right = new Button("right");
        right.setStyle( "-fx-background-color: #f52887");
        down = new Button("down");
        down.setStyle( "-fx-background-color: #f52887");
        HBox justUp = new HBox();
        HBox justDown = new HBox();
        justDown.setPadding(new Insets(0,0,0,50));
        justDown.getChildren().add(down);
        justUp.setPadding(new Insets(50,0,0,50));
        justUp.getChildren().add(up);

        HBox rightLeft = new HBox();
        rightLeft.setSpacing(20);
        rightLeft.setPadding(new Insets(20));
        rightLeft.getChildren().addAll(left, right);
        leftSide.getChildren().addAll(label1, label2,justUp,rightLeft,justDown);
        leftSide.setPadding(new Insets(50));
        leftSide.setPadding(new Insets(20));
        borderPane.setCenter(drawBoard(game.getGameboard().getPieces()[0],
                game.getGameboard().getPieces()[1]));
        return borderPane;
    }

    public Pane drawBoard(Piece piece1, Piece piece2){
        cellsRectangles = new Rectangle[9][9];
        Circle[][] wallsCircles = new Circle[8][8];
        vertical=new Rectangle[10][10];
        horizontal = new Rectangle[10][10];
        Pane pane =new Pane();
        for(int j =0; j<=8; j++) {
            for (int i = 0; i <= 8; i++) {
                cellsRectangles[j][i] = new Rectangle(70 * i, j*70,
                        50, 50);

                cellsRectangles[j][i].setFill(Color.GOLD);
                pane.getChildren().add(cellsRectangles[j][i]);
            }
        }
        for(int j =0; j<=7; j ++) {
            for (int i = 0; i <= 7; i++) {
                wallsCircles[j][i] = new Circle(60*(i+1)+10*(i),
                        (j+1)*60+10*(j),
                        10,  Color.DARKGOLDENROD);
                pane.getChildren().add(wallsCircles[j][i]);
            }
        }
        for(int i =0; i<9; i ++) {
            for (int j = 0; j <8; j++) {
                vertical[i][j] = new Rectangle(50*(j+1) + 20*(j), (i)*70,
                        20,50);
                vertical[i][j].setFill(Color.LIGHTYELLOW);
                pane.getChildren().add(vertical[i][j]);
            }
        }

        for(int i =0; i<8; i ++) {
            for (int j = 0; j <9; j++) {
                horizontal[i][j] = new Rectangle(j*70, (i+1)*50+20*i,
                        50,20);
                horizontal[i][j].setFill(Color.LIGHTYELLOW);
                pane.getChildren().add(horizontal[i][j]);
            }
        }
            cellsRectangles[game.getGameboard().getPieces()[0].getPosition().getRow()]
                    [game.getGameboard().getPieces()[0].getPosition().getCol()].
                    setFill(Color.ORANGERED);
            cellsRectangles[game.getGameboard().getPieces()[1].getPosition().getRow()]
                    [game.getGameboard().getPieces()[1].getPosition().getCol()].
                    setFill(Color.DARKOLIVEGREEN);


           up.setOnMouseClicked(event -> {
               Action action = new Move(Sake.UP);
               setColor(action);
            });
            down.setOnMouseClicked(event -> {
                Action action = new Move(Sake.DOWN);
                setColor(action);
            });

            right.setOnMouseClicked(event -> {
                Action action = new Move(Sake.RIGHT);
                setColor(action);
            });
            left.setOnMouseClicked(event -> {
                Action action = new Move(Sake.LEFT);
                setColor(action);
            });
            for (int i=0; i<8; i++){
                int k=i;
                for (int j = 0; j<8; j++){
                    int l = j;
                    wallsCircles[i][j].setOnMouseClicked(event -> {
                       if( event.getButton() == MouseButton.PRIMARY){
                           Action action = new Block(new Position(k,l ), Direction.HORIZENTAL);
                           setWallColor(action);
                       }else if(event.getButton()==MouseButton.SECONDARY){
                           System.out.println("inelseif");
                           Action action = new Block(new Position(k,l), Direction.VERTICAL);
                           setWallColor(action);
                       }
                    });
                }
            }
        return pane;
    }

    private void setWallColor(Action action) {
            if(!((game.av.actionIsValid(action, game))==null)){
                if(((Block) action).getDirection() == Direction.HORIZENTAL) {
                    horizontal[((Block) action).getPosition().getRow()]
                            [((Block) action).getPosition().getCol()].
                            setFill(Color.DARKGOLDENROD);
                    horizontal[((Block) action).getPosition().getRow()]
                            [((Block) action).getPosition().getCol() + 1].
                            setFill(Color.DARKGOLDENROD);
                }else if(((Block) action).getDirection() == Direction.VERTICAL){
                    vertical[((Block) action).getPosition().getRow()][((Block) action)
                            .getPosition().getCol()].
                            setFill(Color.DARKGOLDENROD);
                    vertical[((Block) action).getPosition().getRow()+1]
                            [((Block) action).getPosition().getCol()].
                            setFill(Color.DARKGOLDENROD);
                }
            }
            game.run(action);
        }


    public void setColor(Action action){
        if(!((game.av.actionIsValid(action,  game))==null)) {
            cellsRectangles[game.getGameboard().getPieces()[game.turn % 2].
                    getPosition().getRow()]
                    [game.getGameboard().getPieces()[game.turn % 2].getPosition().getCol()].
                    setFill(Color.GOLD);
        }
        game.run( action);
        if(game.turn%2==1) {
            cellsRectangles[game.getGameboard().getPieces()[0].getPosition().getRow()]
                    [game.getGameboard().getPieces()[0].getPosition().getCol()].
                    setFill(Color.ORANGERED);
        }else{
            cellsRectangles[game.getGameboard().getPieces()[1].getPosition().getRow()]
                    [game.getGameboard().getPieces()[1].getPosition().getCol()].
                    setFill(Color.DARKOLIVEGREEN);
        }

    }
}