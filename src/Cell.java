import java.util.ArrayList;
import java.util.List;

public class Cell extends BoardItem{
    Position position;

    static List<Cell> cells = new ArrayList<>();

    public Cell(Position position) {
        super(position.getRow(), position.getCol());
    }
    public static List<Cell> getCells(){
        for(int i = 0; i<9; i++){
            for(int j = 0; j<9; j++){
                Cell cell = new Cell(new Position(i,j));
                cells.add(cell);
            }
        }
        return cells;
    }
    public Cell getLowerCell(){
        if(this.getRow()<8) {
            for(Cell cell:cells) {
                if(cell.getRow()==this.getRow()+1 && cell.getCol()==this.getCol()){
                    return cell;
                }
            }
        }
        return null;
    }
    public Cell getRightCell(){
        if(this.getCol()<8){
            for(Cell cell:cells){
                if(cell.getCol() == this.getCol()+1 && cell.getRow() == this.getRow()){
                    return cell;
                }
            }
        }
        return null;
    }
    public Cell getUpperCell(){
        if(this.getRow()>0) {
            for(Cell cell:cells) {
                if(cell.getRow()==this.getRow()-1 && cell.getCol()==this.getCol()){
                    return cell;
                }
            }
        }
        return null;
    }
    public Cell getLeftCell(){
        if(this.getCol()>0){
            for(Cell cell:cells){
                if(cell.getCol() == this.getCol()-1 && cell.getRow() == this.getRow()){
                    return cell;
                }
            }
        }
        return null;
    }
    public static Cell searchCell(Integer row, Integer col){
        for(Cell cell:cells){
            if(cell.getRow() == row && cell.getCol() == col){
                return cell;
            }
        }
        return null;
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "row=" + this.getRow() +
                ", Col=" + this.getCol() +
                '}';
    }
}
