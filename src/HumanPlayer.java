public class HumanPlayer extends Player {

    public HumanPlayer(String name){
        super(name);
    }

    @Override
    public Action nextAction() {
        return new Move(Sake.UP);
    }
}
