

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.BreadthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import javax.swing.*;

public class Game {
    private static GameBoard gameboard;
    static Integer turn;
     ActionValidator av;
     ActionPerform ap ;
    public  Game(){
         av=new ActionValidator();
         ap= new ActionPerform();
        gameboard = new GameBoard();
        turn=0;
    }


    public static GameBoard getGameboard() {
        return gameboard;
    }

    public void run(Action action) {
        if (!(av.actionIsValid(action, this)==null)){
            ap.perform(action, this,av.actionIsValid(action,this));
            turn++;
            System.out.println(turn);
        }else{
            System.out.println("false");
        }
        isGameOver();
    }

    public Boolean isGameOver() {
        hasWon();
        isTrapped();
        return false;
    }

    private boolean hasWon() {
        if(getGameboard().getPieces()[0].getPosition().getRow()==8){
        gameOver(turn,new Label("GameOver!\n"+"Player "+ (turn%2==0? 2:1)+" has Won!"));
            return true;

        }else if(getGameboard().getPieces()[1].getPosition().getRow()==0){
        gameOver(turn,new Label("GameOver!\n"+"Player "+ (turn%2==0?2:1)+" has Won!"));
            return true;
        }
        return false;
    }

    public Boolean isTrapped(){
        System.out.println("inistrapped");
        Integer counter0=0;
        Integer counter1=0;
        GraphIterator<Cell, DefaultEdge> iteratorPlayer0 = new BreadthFirstIterator<>(
           gameboard.getBoard(),
                Cell.searchCell(gameboard.getPieces()[0].getPosition().getRow(),
                gameboard.getPieces()[0].getPosition().getCol()));
        while(iteratorPlayer0.hasNext()){
            Cell cell = iteratorPlayer0.next();
            if(cell.getRow()==8){
                counter0++;
            }
        }

        GraphIterator<Cell, DefaultEdge> iteratorPlayer1 = new BreadthFirstIterator<>(
                gameboard.getBoard(),
                Cell.searchCell(gameboard.getPieces()[1].getPosition().getRow(),
                        gameboard.getPieces()[1].getPosition().getCol()));
        while(iteratorPlayer1.hasNext()){
            Cell cell = iteratorPlayer1.next();
            if(cell.getRow()==0){
                counter1++;
            }
        }

        if(counter0==0 || counter1==0){
            gameOver(turn,new Label("GameOver!\n"+"Player "+ (turn%2+1)+" has Won!"));
            return true;
        }
        return false;
    }

    private void gameOver(Integer turn, Label l) {
 //       Label l = new Label("GameOver!\n"+"Player "+ (turn%2+1)+" has Won!");

        Button ok = new Button("OK");
        VBox vbox = new VBox(l, ok);
        ok.setOnMouseClicked(event -> {
                System.exit(1);});
        ok.setStyle("-fx-background-color: #f52887");
        vbox.setAlignment(Pos.CENTER);
        vbox.setStyle("-fx-background-color: #e8adaa");
        Scene scene = new Scene(vbox, 200,200);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }
}
