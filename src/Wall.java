import java.util.ArrayList;

public class Wall extends BoardItem{
    private Position position;
    private Direction direction;
    ArrayList<Cell> wallNeighbors = new ArrayList<>();
    Cell upLeft = new Cell(new Position(0,0));

    public Wall(Position position, Direction direction) {
        super(position.getRow(), position.getCol());
        this.position = position;
        this.direction = direction;
    }


    public ArrayList<Cell> getWallNeighbors() {
        if(this.getPosition()!= null) {
            for (Cell cell: Cell.getCells()) {
                System.out.println(cell);
                if (cell.getRow()== this.getRow() && cell.getCol()==this.getCol()) {
                    upLeft = cell;
                    wallNeighbors.add(upLeft);
                    wallNeighbors.add(upLeft.getRightCell());
                    wallNeighbors.add(upLeft.getLowerCell());
                    wallNeighbors.add(upLeft.getLowerCell().getRightCell());
                    return wallNeighbors;
                }
            }

        }
       return null;
    }

    public Position getPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "Wall{" +
                "position: row= "+  this.getRow() + ", column= "+this.getCol()+
                ", direction=" + direction +
                '}';
    }
}
