import javafx.util.Pair;

public class ActionValidator {
    private  Cell moving=new Cell(new Position(0,0));
    private  Cell still= new Cell(new Position(0,0));

    public Pair<Integer, Integer> actionIsValid(Action action, Game g){
        moving = Cell.searchCell(g.getGameboard().getPieces()[g.turn % 2].getPosition().getRow(),
                g.getGameboard().getPieces()[g.turn % 2].getPosition().getCol());
        System.out.println("moving"+moving);
        still = Cell.searchCell(g.getGameboard().getPieces()[g.turn%2 == 0?1:0].getPosition().getRow(),
                g.getGameboard().getPieces()[g.turn%2 == 0?1:0].getPosition().getCol() );
        System.out.println("still"+still);
        if (action instanceof Move) {
            switch (((Move) action).sake) {
                case UP:
                    return validateMoveUp(g);
                case DOWN:
                    return validateMoveDown(g);
                case RIGHT:
                    return validateMoveRight(g);
                case LEFT:
                    return validateMoveLeft(g);
            }
        }
        else if(action instanceof Block) {
            return validatePutBlock(action, g);

        }
        return null;
    }

    private Pair<Integer, Integer> validatePutBlock(Action action, Game g) {
        if (g.getGameboard().getWallStatus().get(g.turn % 2 == 0 ? 0 : 1).size() == 11) {
            return null;
        }else if((((Block) action).getPosition().getRow()==8||((Block) action).getPosition().getCol()==8)){
            return null;
        }else{
            for (Wall wall : g.getGameboard().getWallStatus().get(g.turn % 2 == 0 ? 0 : 1)) {
                if (sameWallHandler((Block) action, wall)) return null;
                for (Wall wall1 : g.getGameboard().getWallStatus().get(g.turn % 2 == 0 ? 1 : 0)) {
                    if (sameWallHandler((Block) action, wall1)) return null;
                }
            }
            return new Pair<Integer, Integer>(((Block) action).getPosition().getRow(),
                    ((Block) action).getPosition().getCol());
        }
    }

    private boolean sameWallHandler(Block action, Wall wall1) {
        if (wall1.getRow() == action.getPosition().getRow() &&
                wall1.getCol() == action.getPosition().getCol()) {

            return true;
        } else if ((wall1.getRow() == action.getPosition().getRow() &&
                Math.abs(wall1.getCol() - action.getPosition().getCol()) == 1)
                && (wall1.getDirection() == Direction.HORIZENTAL &&
                action.getDirection() == Direction.HORIZENTAL)) {
            return true;
        } else if ((wall1.getCol() == action.getPosition().getCol() &&
                Math.abs(wall1.getRow() - action.getPosition().getRow()) == 1)
                && (wall1.getDirection() == Direction.VERTICAL &&
                action.getDirection() == Direction.VERTICAL)) {
            return true;
        }
        return false;
    }

    public  Pair<Integer, Integer> validateMoveUp(Game g) {
        if (moving.getRow() > 0) {
            if (!(still.getRow() == moving.getUpperCell().getRow() && still.getCol() == moving.getUpperCell().getCol())) {
                if (g.getGameboard().getBoard().containsEdge(moving, moving.getUpperCell())) {
                    return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol());
                }
            } else if ( (moving.getRow() - 1 > 0) &&(still.getRow() == moving.getUpperCell().getRow() &&
                    still.getCol() == moving.getUpperCell().getCol() &&
                        (g.getGameboard().getBoard().containsEdge(moving, moving.getUpperCell())
                                && g.getGameboard().getBoard().containsEdge(moving.getUpperCell(),
                                moving.getUpperCell().getUpperCell())))) {
                return new Pair<Integer, Integer>(moving.getRow()-2, moving.getCol());

                    } else if ((moving.getRow() - 1 == 0 &&moving.getCol()<8)&&
                            g.getGameboard().getBoard().containsEdge(moving.getUpperCell(),
                                    moving.getUpperCell().getRightCell())) {
                return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol()+1);
                    }
                 else if ((moving.getRow() - 1 == 0 &&moving.getCol()>0)&&
                        g.getGameboard().getBoard().containsEdge(moving.getUpperCell(),
                                moving.getUpperCell().getLeftCell())) {
                return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol()-1);
                }
             else if ((moving.getCol()<8 &&(still.getRow() == moving.getUpperCell().getRow() &&
                    still.getCol() == moving.getUpperCell().getCol())) &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getUpperCell()) &&
                            g.getGameboard().getBoard().containsEdge(moving.getUpperCell(),
                                    moving.getRightCell().getUpperCell()))) {
                return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol()+1);
            } else if ((moving.getCol()>0 && (still.getRow() == moving.getUpperCell().getRow() &&
                    still.getCol() == moving.getUpperCell().getCol())) &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getUpperCell()) &&
                            g.getGameboard().getBoard().containsEdge(moving.getUpperCell(),
                                    moving.getLeftCell().getUpperCell()))) {
                return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol()-1);
            }
        }
        return null;
    }
    public Pair<Integer,Integer> validateMoveDown(Game g) {
        if (moving.getRow() < 8) {
            if (!(still.getRow() == moving.getLowerCell().getRow() && still.getCol() == moving.getLowerCell().getCol())) {
                if (g.getGameboard().getBoard().containsEdge(moving, moving.getLowerCell())) {
                    return new Pair<Integer, Integer>(moving.getRow()+1, moving.getCol());
                }
            } else if ( (moving.getRow() +1 <8) &&(still.getRow() == moving.getLowerCell().getRow()
                    && still.getCol() == moving.getLowerCell().getCol() &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getLowerCell())
                            && g.getGameboard().getBoard().containsEdge(moving.getLowerCell(),
                            moving.getLowerCell().getLowerCell())))) {

                return new Pair<Integer, Integer>(moving.getRow()+2, moving.getCol());

            } else if ((moving.getRow() + 1 == 8 &&moving.getCol()<8)&&
                    g.getGameboard().getBoard().containsEdge(moving.getLowerCell(),
                            moving.getLowerCell().getRightCell())) {
                return new Pair<Integer, Integer>(moving.getRow()+1, moving.getCol()+1);
            }
            else if ((moving.getRow() +1 == 8 && moving.getCol()>0)&&
                    g.getGameboard().getBoard().containsEdge(moving.getLowerCell(),
                            moving.getLowerCell().getLeftCell())) {
                return new Pair<Integer, Integer>(moving.getRow()+1, moving.getCol()-1);
            }
            else if ((moving.getCol()<8 &&(still.getRow() == moving.getLowerCell().getRow() &&
                    still.getCol() == moving.getLowerCell().getCol()) )&&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getLowerCell()) &&
                            g.getGameboard().getBoard().containsEdge(moving.getLowerCell(),
                                    moving.getRightCell().getLowerCell()))) {
                return new Pair<Integer, Integer>(moving.getRow()+1, moving.getCol()+1);
            } else if ((moving.getCol()>0 && (still.getRow() == moving.getLowerCell().getRow() &&
                    still.getCol() == moving.getLowerCell().getCol())) &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getLowerCell()) &&
                            g.getGameboard().getBoard().containsEdge(moving.getLowerCell(),
                                    moving.getLeftCell().getLowerCell()))) {
                return new Pair<Integer, Integer>(moving.getRow()+1, moving.getCol()-1);
            }
        }
        return null;
    }

    public  Pair<Integer, Integer> validateMoveRight(Game g) {
        if (moving.getCol() < 8) {
            if (!(still.getRow() == moving.getRightCell().getRow() && still.getCol() == moving.getRightCell().getCol())) {
                if (g.getGameboard().getBoard().containsEdge(moving, moving.getRightCell())) {
                    return new Pair<Integer, Integer>(moving.getRow(), moving.getCol()+1);
                }
            } else if ( (moving.getCol() +1 <8) &&(still.getRow() == moving.getRightCell().getRow() &&
                    still.getCol() == moving.getRightCell().getCol() &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getRightCell())
                            && g.getGameboard().getBoard().containsEdge(moving.getRightCell(),
                            moving.getRightCell().getRightCell())))) {

                return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol()+2);

            } else if ((moving.getCol() + 1 == 8 && moving.getRow()>0)&&
                    g.getGameboard().getBoard().containsEdge(moving.getRightCell(),
                            moving.getUpperCell().getRightCell())) {
                return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol()+1);
            }
            else if ((moving.getCol() +1 == 8 && moving.getRow()<8)&&
                    g.getGameboard().getBoard().containsEdge(moving.getRightCell(),
                            moving.getLowerCell().getRightCell())) {
                return new Pair<Integer, Integer>(moving.getRow()+1, moving.getCol()+1);
            }
            else if ((moving.getRow()<8&&(still.getRow() == moving.getRightCell().getRow() &&
                    still.getCol() == moving.getRightCell().getCol())) &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getRightCell()) &&
                            g.getGameboard().getBoard().containsEdge(moving.getRightCell(),
                                    moving.getRightCell().getLowerCell()))) {
                return new Pair<Integer, Integer>(moving.getRow()+1, moving.getCol()+1);
            } else if ((moving.getRow()>0&& (still.getRow() == moving.getRightCell().getRow() &&
                    still.getCol() == moving.getRightCell().getCol())) &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getRightCell()) &&
                            g.getGameboard().getBoard().containsEdge(moving.getRightCell(),
                                    moving.getRightCell().getUpperCell()))) {
                return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol()+1);
            }
        }
        return null;
    }
    public Pair<Integer, Integer> validateMoveLeft(Game g) {
        if (moving.getCol() > 0) {
            if (!(still.getRow() == moving.getLeftCell().getRow() && still.getCol() == moving.getLeftCell().getCol())) {
                if (g.getGameboard().getBoard().containsEdge(moving, moving.getLeftCell())) {
                    return new Pair<Integer, Integer>(moving.getRow(), moving.getCol()-1);
                }
            } else if ( (moving.getCol() - 1 > 0) &&(still.getRow() == moving.getLeftCell().getRow()
                    && still.getCol() == moving.getLeftCell().getCol() &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getLeftCell())
                            && g.getGameboard().getBoard().containsEdge(moving.getLeftCell(),
                            moving.getLeftCell().getLeftCell())))) {

                return new Pair<Integer, Integer>(moving.getRow(), moving.getCol()-2);

            } else if ((moving.getCol() - 1 == 0&&moving.getRow()>0) &&
                    g.getGameboard().getBoard().containsEdge(moving.getLeftCell(),
                            moving.getUpperCell().getLeftCell())) {
                return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol()-1);
            }
            else if ((moving.getCol() - 1 == 0 &&moving.getRow()<8)&&
                    g.getGameboard().getBoard().containsEdge(moving.getLeftCell(),
                            moving.getLowerCell().getLeftCell())) {
                return new Pair<Integer, Integer>(moving.getRow()+1, moving.getCol()-1);
            }
            else if ((moving.getRow()>0&&(still.getRow() == moving.getUpperCell().getRow() &&
                    still.getCol() == moving.getUpperCell().getCol()) &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getLeftCell())) &&
                            g.getGameboard().getBoard().containsEdge(moving.getLeftCell(),
                                    moving.getLeftCell().getUpperCell()))) {
                return new Pair<Integer, Integer>(moving.getRow()-1, moving.getCol()-1);
            } else if ((moving.getRow()<8&&(still.getRow() == moving.getLeftCell().getRow() &&
                    still.getCol() == moving.getLeftCell().getCol()) &&
                    (g.getGameboard().getBoard().containsEdge(moving, moving.getLeftCell())) &&
                            g.getGameboard().getBoard().containsEdge(moving.getLeftCell(),
                                    moving.getLeftCell().getLowerCell()))) {
                return new Pair<Integer, Integer>(moving.getRow()+1, moving.getCol()-1);
            }
        }
        return null;
    }

    public Cell getMoving() {
        return moving;
    }

    public Cell getStill() {
        return still;
    }

}
