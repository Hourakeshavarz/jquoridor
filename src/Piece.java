import java.awt.*;

public class Piece extends BoardItem {
    private Position position;
    public Piece(Position position) {
       super(position.getRow(),position.getCol());
       this.position = position;
    }
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
